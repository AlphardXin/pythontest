from selenium import webdriver
import time
from selenium.webdriver.chrome.options import Options


def login_check(username,password):
    chrome_options = Options()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('blink-settings=imagesEnabled=false')
    chrome_options.add_argument('--disable-gpu')
    driver = webdriver.Chrome(options=chrome_options)
    driver.get("http://saas-test.fivebao.com/merchant/#/login")
    time.sleep(2)
    driver.find_element_by_name("username").send_keys(username)
    time.sleep(2)
    driver.find_element_by_name("password").send_keys(password)
    time.sleep(2)
    driver.find_element_by_xpath('//*[@id="app"]/div/div[2]/div[2]/div[1]/form/div[4]/button').click()
    time.sleep(2)

    if 6<=len(password)<=18:
       if username=="17375793583" and password=="12345678":
           return {'code':0,'msg':'登录成功'}
       else:
           return {'code':1,'msg':'请输入账号密码'}

    else:
        return {'code': 1, 'msg': '密码长度在6-18之间'}