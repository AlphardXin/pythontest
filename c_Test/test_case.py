import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
chrome_options = Options()
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')
chrome_options.add_argument('--headless')
driver = webdriver.Chrome(chrome_options=chrome_options)
chrome_options.add_argument('blink-settings=imagesEnabled=false')
chrome_options.add_argument('--disable-gpu')
from c_Test import login_1

class login_test(unittest.TestCase):


    def test_01(self):
        data=login_1.login_check('17375793583','12345678')
        results='登录成功'
        self.assertEqual(data["msg"],results)

    def test_02(self):
        data = login_1.login_check('17375793583', '123')
        results = '密码长度在6-18之间'
        self.assertEqual(data["msg"], results)

    def test_03(self):
        data=data = login_1.login_check('  ', '  ')
        results = '请输入账号密码'
        self.assertEqual(data["msg"], results)

