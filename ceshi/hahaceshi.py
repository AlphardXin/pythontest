from selenium import webdriver
import unittest
from ceshi.haha import login_go
from time import sleep

class Login(unittest.TestCase):
    def testsetUp(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        self.driver.get(login_url)
    def testtearDown(self):
        self.driver.quit()
    def testcase_login(self):
        login_go(self.driver).login("chen","chen")#调用login方法
        sleep(3)
        result1 = self.driver.find_element_by_xpath(".//*[@id='l-topmenu-r-bottm']/span[2]").text
        print(result1)
        result2 = "欢迎您"
        self.assertIn(result2,result1,msg="失败原因：%s中没有发现%s"%(result1,result2))
        sleep(2)
if __name__ == "__main__":
    login_url = "http://www.abc.com"
    unittest.main()