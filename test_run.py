import unittest
from c_Test import test_case
from selenium import webdriver
from BeautifulReport import BeautifulReport
from selenium.webdriver.chrome.options import Options
chrome_options = Options()
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')
chrome_options.add_argument('--headless')
chrome_options.add_argument('blink-settings=imagesEnabled=false')
chrome_options.add_argument('--disable-gpu')
driver = webdriver.Chrome(options=chrome_options)



s=unittest.TestSuite()
s.addTest(test_case.login_test('test_01'))
s.addTest(test_case.login_test('test_02'))
s.addTest(test_case.login_test('test_03'))
#运行
# r=unittest.TextTestRunner()
# r.run(s)

#生成测试报告
r=BeautifulReport(s)
r.report("测试报告")